//
//  Bundle+JsonDecoder.swift
//  VANetworkCallCheck
//
//  Created by Vikash Anand on 04/03/19.
//  Copyright © 2019 Vikash Anand. All rights reserved.
//

import Foundation

extension Bundle {
    
    func decode<T:Decodable>(_ type: T.Type, _ fileName: String) -> T {
        
        let jsonData = self.loadDataFor(fileName: fileName)
        let jsonDecoder = JSONDecoder()
        guard let loaded = try? jsonDecoder.decode(type.self, from: jsonData) else {
            fatalError("Can't decode json data")
        }
        
        return loaded
    }
    
    func loadDataFor(fileName name: String) -> Data {
        
        guard let jsonUrl = self.url(forResource: name, withExtension: nil) else {
            fatalError("Json file not found")
        }
        
        guard let jsonData = try? Data(contentsOf: jsonUrl) else {
            fatalError("Can't convert json file to data")
        }
        
        return jsonData
    }
}
