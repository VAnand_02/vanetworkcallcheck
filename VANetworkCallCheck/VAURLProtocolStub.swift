//
//  VAURLProtocolStub.swift
//  VANetworkCallCheck
//
//  Created by Vikash Anand on 04/03/19.
//  Copyright © 2019 Vikash Anand. All rights reserved.
//

import Foundation

class VAURLProtocolStub: URLProtocol {
    
    static var testURLs = [URL? : Data]()
    
    override class func canInit(with request: URLRequest) -> Bool {
        return true
    }
    
    override class func canonicalRequest(for request: URLRequest) -> URLRequest {
        return request
    }
    
    override func startLoading() {
        
        if let url = self.request.url {
            if let data = VAURLProtocolStub.testURLs[url] {
                self.client?.urlProtocol(self, didLoad: data)
            }
        }
        
        self.client?.urlProtocolDidFinishLoading(self)
    }
    
    override func stopLoading() {
    }
}
