//
//  ViewController.swift
//  VANetworkCallCheck
//
//  Created by Vikash Anand on 04/03/19.
//  Copyright © 2019 Vikash Anand. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //self.fetchUserData()
        self.fetchHotelData()
    }
    
    func fetchUserData() {
        let networkLayer = VANetworkLayer("https://demo2761524.mockable.io/users", nil)
        networkLayer.fetchDataFor(session: URLSession.shared,
                             successHandler: { (users: [Users]) in
                        print(users)}
        ) { (errorString) in
            print(errorString)
        }
    }
    
    func fetchHotelData() {
        let networkLayer = VANetworkLayer("https://demo2761524.mockable.io/hotels", nil)
        networkLayer.fetchDataFor(session: URLSession.shared,
                                  successHandler: { (hotels: Hotels) in
                                    print(hotels)}
        ) { (errorString) in
            print(errorString)
        }
    }
}

