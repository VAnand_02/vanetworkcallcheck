//
//  Users.swift
//  VANetworkCallCheck
//
//  Created by Vikash Anand on 04/03/19.
//  Copyright © 2019 Vikash Anand. All rights reserved.
//

import Foundation

struct Users: Codable {
    
    var id: Int
    var name: String
    var username: String
    var email: String
    var address: Address
    
    struct Address: Codable {
        var street: String
        var suite: String
        var city: String
        var zipcode: String
        var geolocation: Geo
        
        enum CodingKeys: String, CodingKey {
            case street
            case suite
            case city
            case zipcode
            case geolocation = "geo"
        }
        
        struct Geo: Codable {
            var lat: String
            var lng: String
        }
    }
    
    var phone: String
    var website: String
    
    struct Company: Codable {
        var name: String
        var catchPhrase: String
        var bs: String
    }
    
    var company:Company
}
