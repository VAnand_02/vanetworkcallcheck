//
//  VANetworkLayer.swift
//  VANetworkCallCheck
//
//  Created by Vikash Anand on 04/03/19.
//  Copyright © 2019 Vikash Anand. All rights reserved.
//

import Foundation

class VANetworkLayer {
    
    typealias CompletionHandler = (Data?, URLResponse?, Error?) -> Void
    typealias ErrorHandler = (String) -> Void
    
    private var url: URL
    private var header: [String:String]?
    init(_ urlString: String, _ header: [String:String]?) {
        guard let url = URL(string: urlString) else {
            fatalError("Invalid url")
        }
        self.url = url
        self.header = header
    }
    
    func fetchDataFor<T: Decodable>(session: URLSession,
                             successHandler: @escaping (T) -> Void,
                               errorHandler: @escaping ErrorHandler) {
        
        let completionHandler: CompletionHandler = {(data, response, error) in
            
            if let error = error {
                errorHandler(error.localizedDescription)
                return
            }
            
            if !self.isSuccessResponse(response) {
                errorHandler("Invalid response")
            }
            
            guard let data = data else {
                errorHandler("Invalid response")
                return
            }
        
            guard let modelObject = try? JSONDecoder().decode(T.self, from: data) else {
                errorHandler("Can not parse data")
                return
            }
            successHandler(modelObject)
        }
        
        var urlRequest = URLRequest(url: self.url)
        if let header = self.header {
            urlRequest.allHTTPHeaderFields = header
        }
        
        #if STUBBED
            VAStubbedServer.getResponse(forUrl: self.url, with: completionHandler)
        #else
            session.dataTask(with: urlRequest,
                completionHandler: completionHandler).resume()
        #endif
    }
    
    private func isSuccessResponse(_ response: URLResponse?) -> Bool {
        guard let response = response as? HTTPURLResponse else {
            return false
        }
        return self.isSuccessCode(response.statusCode)
    }
    
    private func isSuccessCode(_ statusCode: Int) -> Bool {
        return statusCode >= 200 && statusCode < 300
    }
}
