//
//  VAStubbedServer.swift
//  VANetworkCallCheck
//
//  Created by Vikash Anand on 04/03/19.
//  Copyright © 2019 Vikash Anand. All rights reserved.
//

import Foundation

enum VANetworkError: Error {
    case StubbedResponseNotAvailable
}

class VAStubbedServer {
    
    class func getResponse(forUrl url: URL,
               with completionHandler:(Data?, URLResponse?, Error?)->()){
        if url.absoluteURL.lastPathComponent == "users" {
            let response = Bundle.main.loadDataFor(fileName: "Users.json")
            completionHandler(response, nil, nil)
            return
        } else if (url.absoluteURL.lastPathComponent == "hotels") {
            let response = Bundle.main.loadDataFor(fileName: "Hotels.json")
            completionHandler(response, nil, nil)
            return
        }
        completionHandler(nil, nil, VANetworkError.StubbedResponseNotAvailable)
    }
}
