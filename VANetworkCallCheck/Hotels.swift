//
//  Hotels.swift
//  VANetworkCallCheck
//
//  Created by Vikash Anand on 04/03/19.
//  Copyright © 2019 Vikash Anand. All rights reserved.
//

import Foundation

struct Hotels: Codable {
    
    struct Location: Codable {
        var name: String
        var position: [Float]
    }
    
    var locations: [Location]
}
