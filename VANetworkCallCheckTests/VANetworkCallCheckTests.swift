//
//  VANetworkCallCheckTests.swift
//  VANetworkCallCheckTests
//
//  Created by Vikash Anand on 04/03/19.
//  Copyright © 2019 Vikash Anand. All rights reserved.
//

import XCTest
@testable import VANetworkCallCheck

class VANetworkCallCheckTests: XCTestCase {

    var hotels: Hotels?
    var users: [Users]?
    private static var configuration: URLSessionConfiguration?
    
    override class func setUp() {
        super.setUp()
        
        // now set up a configuration to use our mock
        VANetworkCallCheckTests.configuration = URLSessionConfiguration.ephemeral
        VANetworkCallCheckTests.configuration?.protocolClasses = [VAURLProtocolStub.self]
    }
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    override class func tearDown() {
        super.tearDown()
        
        VANetworkCallCheckTests.configuration = nil
    }
    
    func testFetchUsersData() {
        
        // this is the URL we expect to call
        let url = URL(string: "https://demo2761524.mockable.io/users")
        
        // attach that to some fixed data in our protocol handler
        VAURLProtocolStub.testURLs = [url: Bundle.main.loadDataFor(fileName: "Users.json")]
        
        // and create the URLSession from that
        if let configuration = VANetworkCallCheckTests.configuration {
            
            let session = URLSession(configuration: configuration)
            let exp = self.expectation(description: "Users json loading check")
            let networkLayer = VANetworkLayer("https://demo2761524.mockable.io/users", nil)
            networkLayer.fetchDataFor(session: session,
                                      successHandler: { [weak self] (users:[Users]) in
                                        if let self = self {
                                            self.users = users
                                        }
                                        exp.fulfill()
                })
            { (error) in
                print(error)
            }
            
            self.waitForExpectations(timeout: 5)
            XCTAssertEqual(self.users?.count, 10)
        }
    }
    
    func testFetchHotelsData() {
        
        // this is the URL we expect to call
        let url = URL(string: "https://demo2761524.mockable.io/hotels")
        
        // attach that to some fixed data in our protocol handler
        VAURLProtocolStub.testURLs = [url: Bundle.main.loadDataFor(fileName: "Hotels.json")]
        
        // and create the URLSession from that
        if let configuration = VANetworkCallCheckTests.configuration {
            
            let session = URLSession(configuration: configuration)
            let exp = self.expectation(description: "Hotels json loading check")
            let networkLayer = VANetworkLayer("https://demo2761524.mockable.io/hotels", nil)
            networkLayer.fetchDataFor(session: session,
                                      successHandler: { [weak self] (hotels: Hotels) in
                                        if let self = self {
                                            self.hotels = hotels
                                        }
                                        exp.fulfill()
                })
            { (error) in
                print(error)
            }
            
            self.waitForExpectations(timeout: 5)
            
            XCTAssertEqual(self.hotels?.locations.count, 3)
            XCTAssertEqual(self.hotels?.locations.count, 3)
            XCTAssertEqual(self.hotels?.locations.first?.name, "Rixos The Palm Dubai")
            XCTAssertEqual(self.hotels?.locations.first?.position.count, 2)
            XCTAssertEqual(self.hotels?.locations.first?.position, [25.1212, 55.1535])
        }
    }
}
